import pandas, numpy
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix

data = pandas.read_csv('trainingData.csv')

targets = list(data.columns.values)
targets.remove('RoomName')

accuracy = []
for i in range(1, 30):
    data = shuffle(data)
    data.reset_index(inplace=True, drop=True)

    X = data[targets]
    y = data[['RoomName']]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    scaler = StandardScaler()
    scaler.fit(X_train)

    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)

    classifier = KNeighborsClassifier(n_neighbors=i)
    classifier.fit(X_train, numpy.array(y_train).ravel())
    y_pred = classifier.predict(X_test)

    yp = list(y_pred)
    yt = list(y_test['RoomName'])

    res = []
    for j in range(len(yp)):
        if yp[j] != yt[j]:
            res.append(0)
        else:
            res.append(1)
    accuracy.append(numpy.mean(res))

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

pp = PdfPages('accplot.pdf')
plt.figure(figsize=(12, 6))
plt.plot(range(1, len(accuracy)+1), accuracy, linestyle='dashed', marker='o', markerfacecolor='blue', markersize=10)
plt.xlabel('K Value')
plt.ylabel('Accuracy')
plt.grid(True)
pp.savefig()
pp.close()
plt.close()