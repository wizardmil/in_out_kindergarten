import pandas, random

df = pandas.read_csv('data1.csv')

setpoints = {}

for location in df.RoomName:
    
    if location not in setpoints.keys():
        setpoints.update({location:{}})

    routers = {}
    for router in df.columns.values:

        if router == 'RoomName':
            continue

        current_data = df[df.RoomName == location][router]

        setpoints[location].update({router:{'min':min(current_data), 'max':max(current_data)}})


synthetic = {'RoomName':[]}
print(setpoints['skrivebord'])

for key in setpoints.keys():

    for i in range(200):
        synthetic['RoomName'].append(key)

        for sensor in setpoints[key].keys():

            if sensor not in synthetic.keys():
                synthetic.update({sensor : []})
            
            _min = int(setpoints[key][sensor]['min'])*-1
            _max = int(setpoints[key][sensor]['max'])*-1

            temp = _min
            if _min > _max:
                _min = _max
                _max = temp

            rssi = random.randint(_min, _max)

            synthetic[sensor].append(-rssi)


new_df = pandas.DataFrame.from_dict(synthetic)

new_df.to_csv('training.csv')
# print(df)