from sklearn.decomposition import PCA
import pandas, numpy
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

pp = PdfPages('pcaplot.pdf')

def explained_variance(percentage, dataset): 
    if percentage is None:
        pca = PCA()
    else:
        pca = PCA(percentage)
    pca.fit(dataset)
    components = pca.transform(dataset)
    approx_original = pca.inverse_transform(components)
    return approx_original, pca.n_components_, pca

data = pandas.read_csv('trainingData.csv')
vectors = list(data.columns.values)
vectors.remove('RoomName')
targets = ['RoomName']

classes = data[targets]
data = data[vectors]

data, nr, pca = explained_variance(0.70, data)
org = pca.fit_transform(data)

for cls in list(set(classes['RoomName'])):
    indexes = list(classes[classes.RoomName == cls].index)

    current_class = []
    for i in indexes:
        current_class += [list(org[i])]

    components = numpy.array(current_class)*0.6
    plt.scatter(components[:, 0], components[:, 1], label=str(cls), s=40)

plt.legend()
plt.grid(True)
plt.ylabel('PCA 1')
plt.xlabel('PCA 2')
pp.savefig()
pp.close()
plt.close()
