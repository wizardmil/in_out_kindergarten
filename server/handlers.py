import asyncio, json
import face_recognition
import PIL
import PIL.Image
import base64, struct
from aiohttp import web
from aiohttp_jinja2 import render_template


class Handlers:

    def __init__(self):
        self.nr_images = 0
        self.routes =   [
                            web.get('/', self.main),
                            web.get('/Remove/{id}', self.remove), 
                            web.post('/Register', self.register), 
                            web.get('/GetAllKids', self.getAllKids),
                            web.post('/RecognizeFaces', self.recognizeFaces)
                        ]

        self.kids =[
            {"id":1,
            "name":"Alice",
            "picture": "THIS IS NOT A PICTURE",
            "isInKindergarten":False,
            "pictureEncoding":None},
                {"id":2,
            "name":"Bob",
            "picture": "THIS IS NOT A PICTURE",
            "isInKindergarten":True,
            "pictureEncoding":None},
                {"id":3,
            "name":"Charlie",
            "picture": "THIS IS NOT A PICTURE",
            "isInKindergarten":False,
            "pictureEncoding":None},
            {"id":23,
            "name":"Whaleboy 🐋",
            "picture": "THIS IS NOT A PICTURE",
            "isInKindergarten":False,
            "pictureEncoding":None}
        ]

    async def main(self, request):
        response = render_template("index.html", request, {})
        response.headers['Content-Language'] = 'en'
        return response
    
    async def remove(self, request):
        id = request.match_info.get('id', 'anon')
        print(id)
        for kid in self.kids:
            if str(kid['id']) == str(id):
                self.kids.remove(kid)

        response = render_template("index.html", request, {})
        response.headers['Content-Language'] = 'en'
        return response

    async def register(self, request):
        requestPayload = await request.post()
        image = requestPayload.get("picture").file.read()
        image64 = base64.b64encode(image)
        facepicture= face_recognition.load_image_file(requestPayload.get("picture").file)
        face_encoding = face_recognition.face_encodings(facepicture)[0]
        self.kids.append(
            {"id":len(self.kids), 
            "name":requestPayload['name'] , 
            "picture":image64.decode('UTF-8'), 
            "isInKindergarten":False,
            "pictureEncoding":face_encoding })
        response = render_template("index.html", request, {})
        response.headers['Content-Language'] = 'en'
        return response


    async def recognizeFaces(self, request):        
        payload = await request.read()
        fr = face_recognition
        # image is written to the filesystem
        payload = await request.read()
        filename = 'temp/test.jpg'
        with open(filename, 'wb') as f:
            f.write(payload)
        
        # image is loaded from the filesystem
        facepicture = fr.load_image_file(filename)
        unknown_face_encoding = fr.face_encodings(facepicture)[0]
        
        for kid in self.kids:
            if(kid['pictureEncoding'] is not None):
                results = fr.compare_faces([kid['pictureEncoding']],\
                                                unknown_face_encoding)
                if results[0] == True:
                    kid['isInKindergarten'] = not kid['isInKindergarten']

        return web.Response(text='success')

    def without_key(self, d, key):
        return {x: d[x] for x in d if x != key}

    async def getAllKids(self, request):
        bobe = [self.without_key(k,"pictureEncoding") for k in self.kids]
        result = {"kids": bobe}
        return web.json_response(result)