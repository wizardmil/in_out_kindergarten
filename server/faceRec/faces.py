import face_recognition
import os
import time

listOfFacesOfMe = []

print(time.time())

for filename in os.listdir('known'):
    picture_of_me = face_recognition.load_image_file("known/"+filename)
    my_face_encoding = face_recognition.face_encodings(picture_of_me)[0]
    listOfFacesOfMe.append(my_face_encoding)
print(time.time())
# my_face_encoding now contains a universal 'encoding' of my facial features that can be compared to any other picture of a face!

unknown_picture = face_recognition.load_image_file("unknown/unknown4.jpg")
unknown_face_encoding = face_recognition.face_encodings(unknown_picture)[0]
unknown_picture2 = face_recognition.load_image_file("unknown/unknown5.jpg")
unknown_face_encoding2 = face_recognition.face_encodings(unknown_picture)[0]
print(time.time())
# Now we can see the two face encodings are of the same person with `compare_faces`!

results = face_recognition.compare_faces(listOfFacesOfMe, unknown_face_encoding)
results2 = face_recognition.compare_faces(listOfFacesOfMe, unknown_face_encoding2)
print(time.time())
if results[0] == True:
    print("It's a picture of me!")
else:
    print("It's not a picture of me!")

    print(time.time())
if results2[0] == True:
    print("It's a picture of me!")
else:
    print("It's not a picture of me!")