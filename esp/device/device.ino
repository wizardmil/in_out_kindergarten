// requires espressif

#include "esp_camera.h"
#include "esp_timer.h"
#include <WiFi.h>
#include <base64.h>

#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

const char* ssid = "wifi name"
const char* pass = "wifi password";

int status = WL_IDLE_STATUS;
IPAddress server(192,168,1,28);
int port = 8000;

WiFiClient client;

void initCamera(){
    camera_config_t config;
    config.ledc_channel = LEDC_CHANNEL_0;
    config.ledc_timer = LEDC_TIMER_0;
    config.pin_d0 = Y2_GPIO_NUM;
    config.pin_d1 = Y3_GPIO_NUM;
    config.pin_d2 = Y4_GPIO_NUM;
    config.pin_d3 = Y5_GPIO_NUM;
    config.pin_d4 = Y6_GPIO_NUM;
    config.pin_d5 = Y7_GPIO_NUM;
    config.pin_d6 = Y8_GPIO_NUM;
    config.pin_d7 = Y9_GPIO_NUM;
    config.pin_xclk = XCLK_GPIO_NUM;
    config.pin_pclk = PCLK_GPIO_NUM;
    config.pin_vsync = VSYNC_GPIO_NUM;
    config.pin_href = HREF_GPIO_NUM;
    config.pin_sscb_sda = SIOD_GPIO_NUM;
    config.pin_sscb_scl = SIOC_GPIO_NUM;
    config.pin_pwdn = PWDN_GPIO_NUM;
    config.pin_reset = RESET_GPIO_NUM;
    config.xclk_freq_hz = 20000000;
    config.pixel_format = PIXFORMAT_JPEG; 

    if (psramFound()) {
        config.frame_size = FRAMESIZE_UXGA;
        config.jpeg_quality = 10;
        config.fb_count = 2;
    } else {
        config.frame_size = FRAMESIZE_SVGA;
        config.jpeg_quality = 12;
        config.fb_count = 1;
    }

    esp_err_t err = esp_camera_init(&config);
    if (err != ESP_OK) {
        Serial.printf("Camera init failed with error 0x%x", err);
    }

    sensor_t * s = esp_camera_sensor_get();

    if (s->id.PID == OV3660_PID) {
        s->set_vflip(s, 1);//flip it back
        s->set_brightness(s, 1);//up the blightness just a bit
        s->set_saturation(s, -2);//lower the saturation
    }

    //drop down frame size for higher initial frame rate
    s->set_framesize(s, FRAMESIZE_QVGA);
    Serial.println("Camera initialized.");
}

camera_fb_t* snapPhoto(){
    camera_fb_t * fb = NULL;
    esp_err_t res = ESP_OK;
    int64_t fr_start = esp_timer_get_time();

    fb = esp_camera_fb_get();  
    
    if(!fb) {
        Serial.println("Camera capture failed");
        return NULL;
    }
    size_t fb_len = 0;
    fb_len = fb->len;
    int64_t fr_end = esp_timer_get_time();
    Serial.printf("JPG info: %uB %ums\n", (uint32_t)(fb_len), (uint32_t)((fr_end - fr_start)/1000));

    esp_camera_fb_return(fb);
    return fb;
}

void connectToWiFi(){
    WiFi.begin(ssid, pass);
    Serial.print("Establishing WiFi connection.");
    while (WiFi.status() != WL_CONNECTED) { 
        Serial.print(".");
        delay(500);
    }
    Serial.println("");
    Serial.println("Device connected to the Internet.");
}

void connectClient(){
    while (!client.connected()){
        client.connect(server, port);
        Serial.print(".");
        delay(500);
    }
    Serial.println("");
    Serial.println("Connected to server.");
}

void setup(){
    Serial.begin(115200);
    initCamera();
    connectToWiFi();
}

void loop(){
    if(client.connected()){
        camera_fb_t* imageBuffer = snapPhoto();

        if (imageBuffer != NULL){
            String str_size = ((String)(uint32_t)(imageBuffer->len));
            String header = "POST /RecognizeFaces HTTP/1.1\r\nHost: " + server.toString() +"\r\nConnection: close\r\nContent-Length: " + str_size + "\r\nContent-Type: image/jpeg\r\n\r\n";
            
            client.print(header);
            size_t size = imageBuffer->len;
            client.write_P((char*) imageBuffer->buf, size);
            Serial.println("Transmitted data to server.");
        } else {
            Serial.println("Nothing sent to server.");
        }
    } else {
        Serial.print("Connecting client.");
        connectClient();
    }

    delay(2000);
}