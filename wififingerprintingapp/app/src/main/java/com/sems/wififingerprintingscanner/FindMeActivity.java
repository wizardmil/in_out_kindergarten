package com.sems.wififingerprintingscanner;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class FindMeActivity extends Activity {

    private static final String TAG = FindMeActivity.class.getSimpleName();

    private static final long WIFI_SCAN_DELAY_MILLIS = TimeUnit.SECONDS.toMillis(1);

    private WifiManager wifiManager;
    private WifiScanBroadcastReceiver wifiScanBroadcastReceiver = new WifiScanBroadcastReceiver();
    private WifiLock wifiLock;

    private volatile boolean running;
    LocationManager locationManager;

    Map<String, List<String>> wifiStrengths = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_me);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        checkLocationPermission();


    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    public void buttonClicked(View e){
        Log.e("AAAAA", "THIS IS BUTTON");
        ((TextView)findViewById(R.id.textView)).setText("SCANNING");

        doScan();
    }

    private void doScan(){

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_SCAN_ONLY, FindMeActivity.class.getName());
        running = true;
        if(!wifiLock.isHeld()){

            wifiLock.acquire();
        }

        registerReceiver(wifiScanBroadcastReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        final Handler wifiScanHandler = new Handler();
        Runnable wifiScanRunnable = new Runnable() {

            @Override
            public void run() {
                if (!running) {
                    return;
                }

                    if (!wifiManager.startScan()) {
                    Log.w(TAG, "Couldn't start Wi-fi scan!");
                }

                wifiScanHandler.postDelayed(this, WIFI_SCAN_DELAY_MILLIS);
            }

        };
        wifiScanHandler.post(wifiScanRunnable);


    }

    @Override
    protected void onPause() {
        running = false;

        unregisterReceiver(wifiScanBroadcastReceiver);

        wifiLock.release();
        super.onPause();
    }

    private final class WifiScanBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (!running || !WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {
                return;
            }

            List<ScanResult> scanResults = wifiManager.getScanResults();

            Measurement currentSignals = constructMeasurement(scanResults);
            running = false;
            Log.e(TAG,"Got here!");



            List<Measurement> radioMap = readMapFromFile();

            double minimumDistance = 9999999d;
            Measurement pointWithMinDist = null;

            // For each point in radioMap
            for(Measurement point : radioMap){
                double dist = distance(currentSignals, point);
                if(dist < minimumDistance || minimumDistance == 9999999d){
                    minimumDistance = dist;
                    pointWithMinDist = point;
                }
            }

            ((TextView)findViewById(R.id.textView)).setText(pointWithMinDist.roomName);
    // Show on screen        pointWithMinDist;








        }

    }

    private Measurement constructMeasurement(List<ScanResult> scanResults) {
        Measurement result = new Measurement();
        result.wifiStrenghts = new HashMap<>();
        for(ScanResult sr : scanResults){
            result.wifiStrenghts.put(sr.BSSID,sr.level);
        }
        return result;
    }


    private List<Measurement> readMapFromFile(){
        File file = new File(getApplicationContext().getFilesDir(), "testData.csv");
        List<Measurement> results = new ArrayList<>();
        try{

            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            String[] header = br.readLine().split(",");
            String line;
            while ((line =br.readLine()) != null){
                String[] lineElemts = line.split(",");
                Measurement ms = new Measurement();
                ms.wifiStrenghts = new HashMap<>();
                for(int i = 0; i < lineElemts.length;i++){
                    if("RoomName".equals(header[i])){
                        ms.roomName = lineElemts[i];

                    }else{
                        String headerValue = header[i];
                        ms.wifiStrenghts.put(headerValue, Integer.parseInt(lineElemts[i]));
                    }
                }
                results.add(ms);

            }
        }catch (FileNotFoundException e){

        } catch(IOException e2){

        }

        return results;

    }



    private double distance(Measurement A, Measurement B){
        double totaldistance = 0d;
        for(String key : A.wifiStrenghts.keySet()){
            if(!B.wifiStrenghts.containsKey(key)){
                totaldistance+= Math.pow(100,2);
            }else {
                Integer aStrength = A.wifiStrenghts.get(key);
                Integer bStrength = B.wifiStrenghts.get(key);
                totaldistance += Math.pow((aStrength - bStrength), 2);
            }
        }
        return Math.sqrt(totaldistance);
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {



        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Title")
                        .setMessage("text")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(FindMeActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    public boolean checkStoragePermission() {


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Title")
                        .setMessage("text")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(FindMeActivity.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
            return false;
        } else {
            return true;
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                     //   locationManager.requestLocationUpdates("someString", 400, 1, this);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

}