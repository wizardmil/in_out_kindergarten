package com.sems.wififingerprintingscanner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.Manifest;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class WifiActivity extends Activity {

    private static final String TAG = WifiActivity.class.getSimpleName();

    private static final long WIFI_SCAN_DELAY_MILLIS = TimeUnit.SECONDS.toMillis(1);

    private WifiManager wifiManager;
    private WifiScanBroadcastReceiver wifiScanBroadcastReceiver = new WifiScanBroadcastReceiver();
    private WifiLock wifiLock;

    private volatile boolean running;
    LocationManager locationManager;

    Map<String, List<String>> wifiStrengths = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        checkLocationPermission();


    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    public void buttonClicked(View e){
        Log.e("AAAAA", "THIS IS BUTTON");
        ((TextView)findViewById(R.id.scanStatus)).setText("SCANNING");

        doScan();
    }

    public void exportMap(View e){
        Log.e("AAAAA", "THIS IS BUTTON2");

        convertMapToString();

    }

    public void convertMapToString(){

        String headerString = "";


        for(String key : wifiStrengths.keySet()){
            headerString += key + ",";
        }
        String contentString = headerString.substring(0,headerString.length()-1) + "\n";

        for(int i = 0; i < wifiStrengths.get("RoomName").size(); i++) {
            String rowString = "";
            for (String key : wifiStrengths.keySet()) {
                String value = wifiStrengths.get(key).get(i);

                rowString += value + ",";
            }
            contentString += rowString.substring(0, rowString.length() - 1) + "\n";
        }
        saveExternally("testData", contentString);
    }

    private void doScan(){

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_SCAN_ONLY, WifiActivity.class.getName());
        running = true;

        if(!wifiLock.isHeld()){

        wifiLock.acquire();
        }

        registerReceiver(wifiScanBroadcastReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        final Handler wifiScanHandler = new Handler();
        Runnable wifiScanRunnable = new Runnable() {

            @Override
            public void run() {
                if (!running) {
                    return;
                }

                    if (!wifiManager.startScan()) {
                    Log.w(TAG, "Couldn't start Wi-fi scan!");
                }

                wifiScanHandler.postDelayed(this, WIFI_SCAN_DELAY_MILLIS);
            }

        };
        wifiScanHandler.post(wifiScanRunnable);


    }

    @Override
    protected void onPause() {
        running = false;

        unregisterReceiver(wifiScanBroadcastReceiver);

        wifiLock.release();
        super.onPause();
    }

    private final class WifiScanBroadcastReceiver extends BroadcastReceiver {

        int count = 0;
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!running || !WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {
                return;
            }

            List<ScanResult> scanResults = wifiManager.getScanResults();
            running = false;
            Log.e(TAG,"Got here!");
            // Do something with your scanResults
            int x = 2;
            String roomname = ((TextView) findViewById(R.id.RoomName)).getText().toString();

            if(!wifiStrengths.containsKey("RoomName")){
                wifiStrengths.put("RoomName", new ArrayList<String>());
            }
            wifiStrengths.get("RoomName").add(roomname);


            for(ScanResult result : scanResults){
                String name = result.BSSID;
                int strength = result.level;

                if(!wifiStrengths.containsKey(name)){
                    wifiStrengths.put(name, new ArrayList<String>());
                }

                int toPut = checkIndexes(name)-1;
                for( int i = 0; i < toPut; i++){
                    wifiStrengths.get(name).add("-100");
                }
                if(strength > -100){

                wifiStrengths.get(name).add(Integer.toString(strength));
                }else{
                    wifiStrengths.get(name).add(Integer.toString(-100));

                }

            }

            for(String key : wifiStrengths.keySet()){
                int toPut = checkIndexes(key);
                for( int i = 0; i < toPut; i++){
                    wifiStrengths.get(key).add("-100");
                }
            }

            ((TextView)findViewById(R.id.scanStatus)).setText("SCAN DONE   " + ++count);







            wifiLock.release();


        }

    }

    private int checkIndexes (String name){
        int maxLength = 0;
        for(String key : wifiStrengths.keySet()){
            int length = wifiStrengths.get(key).size();
            if( length > maxLength)
                maxLength = length;
        }

        int numberOfThingsToInsert = maxLength-wifiStrengths.get(name).size();
        return numberOfThingsToInsert;
    }

    public boolean saveExternally(String fileName, String data){
        if( Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            File  root = Environment.getExternalStorageDirectory();
            File file = new File(getApplicationContext().getFilesDir(), fileName+".csv");


            try {

                FileWriter writer = new FileWriter(file);

                writer.write(data);

                writer.flush();
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }



            Log.d("DEBUGGING-INFO", "Files Successfully exported.");
            return true;
        }
        return false;
    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {



        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Title")
                        .setMessage("text")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(WifiActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    public boolean checkStoragePermission() {


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Title")
                        .setMessage("text")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(WifiActivity.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
            return false;
        } else {
            return true;
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                     //   locationManager.requestLocationUpdates("someString", 400, 1, this);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

}