package com.sems.wififingerprintingscanner

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


    }

    fun buttonClicked(e: View){
        val intent = Intent(this, WifiActivity::class.java)
        startActivity(intent);

    }

    fun findMyLocation(e:View) {
        val intent = Intent(this, FindMeActivity::class.java)
        startActivity(intent);

    }


}
