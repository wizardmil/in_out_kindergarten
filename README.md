# in_out_kindergarten

# server API:
This section descibes the API of the server. For now only the services needed by the web client have been described. Anything that has a Name() <-- and parentheses are methods/services.
Anything after a : is a type.
Anything with a TypeName{} <-- and the curly boys is the description of a type.

GetStatusOfAllKids() : StatusResult { List\<Kid> kidStatuses}

Kid{
    id: int,
    name: string,
    picture: byte[],
    isIn: bool
}

RegisterKid(request: RegisterKidRequest) : void

RegisterKidRequest{
    name: string,
    picture: byte[]
}

RemoveKid(request: RemoveKidRequest): void

RemoveKidRequest{
    id:int
}

